This is partial replication of SPSS analysis of survey data on the public attitudes towards various sources of electric energy in Saskatchewan collected by the University of Saskatchewan Johnson-Shoyama Graduate School of Public Policy. 

The purpose is to graph the data for publication and to re-calculate Fisher’s Exact Test with confidence intervals. 

All data in this repository is proprietary. The repository is private. If you somehow gained access to this repository without being granted access by the repository's maintainer, please inform the maintainer, Petr Baranovskiy, immediately at: petr.baranovskiy@usask.ca and/or at: baranovskypd@gmail.com

